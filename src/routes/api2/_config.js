import redis from 'redis';
const client = redis.createClient();
const config = {
  host: 'http://127.0.0.1:3000',
  redisClient: client,
};

export default config;