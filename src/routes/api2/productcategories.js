import config from './_config.js';
import axios from 'axios';
const host = config.host;
const url = host + '/api2/productcategories';
const client = config.redisClient;

export async function get(req, res) {
  const token = req.session.token;
  if(!token) {
    return false;
  }
  res.writeHead(200, {
    'content-type': 'text/plain;charset=utf8'
  });
  client.get(url, async function (err, response) {
    if (err) {
      console.log(err);
    }
    if (response) {
      return res.end(response);
    }
    try {
      const result = await axios({
        method: 'get',
        url: url,
        headers: {
          'Authorization': 'Bearer ' + token,
        },
        dataType: 'json'
      });
      let data = result.data;
      client.setex(url, 600, JSON.stringify(data));
      res.end(JSON.stringify(data));
    } catch (error) {
      res.end(JSON.stringify({ error: error.message }));
    }
  })
}
export async function put(req, res) {
  const token = req.session.token;
  if(!token) {
    return false;
  }
  res.writeHead(200, {
    'content-type': 'text/plain;charset=utf8'
  });
  const title = req.body.title;
  const id = req.body.id;
  try {
    const result = await axios({
      method: 'put',
      url: url,
      headers: {
        'Authorization': 'Bearer ' + token,
      },
      data: {
        title: title,
        id: id,
      },
      dataType: 'json'
    });
    let data = result.data;
    if(data.success==0) {
      return res.end(JSON.stringify(data));
    } else {
      client.del(url);
      return res.end(JSON.stringify(data));
    }
  } catch (error) {
    console.log(error);
    res.end(JSON.stringify({ error: error.message }));
  }
}
export async function post(req, res) {
  const token = req.session.token;
  if(!token) {
    return false;
  }
  res.writeHead(200, {
    'content-type': 'text/plain;charset=utf8'
  });
  const title = req.body.title;
  try {
    const result = await axios({
      method: 'post',
      url: url,
      headers: {
        'Authorization': 'Bearer ' + token,
      },
      data: {
        title: title,
      },
      dataType: 'json'
    });
    let data = result.data;
    if(data.success==0) {
      return res.end(JSON.stringify(data));
    } else {
      client.del(url);
      return res.end(JSON.stringify(data));
    }
  } catch (error) {
    console.log(error);
    res.end(JSON.stringify({ error: error.message }));
  }
}
export async function del(req, res) {
  const token = req.session.token;
  if(!token) {
    return false;
  }
  res.writeHead(200, {
    'content-type': 'text/plain;charset=utf8'
  });
  const id = req.query.id;
  try {
    const result = await axios({
      method: 'delete',
      url: url + '?id=' + id,
      headers: {
        'Authorization': 'Bearer ' + token,
      },
      dataType: 'json'
    });
    let data = result.data;
    if(data.success==0) {
      return res.end(JSON.stringify(data));
    } else {
      client.del(url);
      return res.end(JSON.stringify(data));
    }
  } catch (error) {
    console.log(error);
    res.end(JSON.stringify({ error: error.message }));
  }
}