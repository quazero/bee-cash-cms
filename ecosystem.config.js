module.exports = {
  apps: [
    {
      name: "jizhang_cms",
      script: "./__sapper__/build",
      env: {
        PORT: 3030,
        NODE_ENV: "development"
      },
      env_uat: {
        PORT: 3030,
        NODE_ENV: "uat"
      },
      env_production: {
        PORT: 3004,
        NODE_ENV: "production"
      }
    }
  ]
}
